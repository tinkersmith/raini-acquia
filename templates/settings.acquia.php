<?php

// phpcs:ignoreFile

/**
 * @file
 * Settings file overrides for Acquia hosted environments.
 *
 * Ensures that the Acquia hosted settings are being added to the Drupal site
 * configurations. This should include connecting services, enabling Acquia
 * specific functionality (modules) and settings.
 */

/**
 * Define the locations of composer provided Acquia files.
 *
 * These locations are relative to the "vendor" folder, which is generated and
 * updated through the project file generator.
 */
$acquia_blt_settings = '../vendor/acquia/blt/settings/blt.settings.php';
$acquia_memcache_settings = '../vendor/acquia/memcache-settings/memcache.settings.php';

/**
 * Apply Acquia additional settings.
 *
 * Prefer the BLT settings if the Composer dependency is included by Composer.
 * This includes the memcache settings and other optional Acquia settings.
 *
 * Fallback to just including the Acquia memcache recommended settings from
 * "acquia/memcache-settings" directly if additional BLT features are
 * not required.
 */
if (file_exists($acquia_blt_settings)) {
  require $acquia_blt_settings;
}
elseif (file_exists($acquia_memcache_settings)) {
  require $acquia_memcache_settings;
}

/**
 * Apply Acquia Solr Connector plugin to any defined Solr cores.
 * The connector server cores are defined in the Toolshed Search module and can
 * be overridden here by providing a "plugin_id" value of the replacement
 * SolrConnector plugin, and with override settings.
 *
 * This makes it easy to override Solr Connectors per environment using settings
 * for Solr cores name instead relying on the config names for views or
 * Search API server config IDs.
 *
 * @see https://git.drupalcode.org/project/toolshed/-/tree/2.0.x/modules/toolshed_search
 */
if (!empty($settings['solr_core_connector.hosts']) && file_exists($app_root . '/modules/contrib/acquia_search')) {
  foreach ($settings['solr_core_connector.hosts'] as &$solrCore) {
    $solrCore['plugin_id'] = 'solr_acquia_connector';
    $solrCore['plugin_overrides'] = [];
  }
  unset($solrCore);
}

/**
 * ClamAV is an open source anti-virus that can be used for scanning files on
 * the server. The Drupal module makes it available for scanning uploaded files
 * and prevent them from being saved if issues are detected.
 *
 * Acquia makes ClamAV on their cloud hosting and these settings allow the
 * Drupal module to make use of it if it is installed.
 *
 * @see https://drupal.org/project/clamav
 * @see https://docs.acquia.com/cloud-platform/manage/antivirus/
 */
# $config['clamav.settings']['scan_mode'] = 1;
# $config['clamav.settings']['mode_executable']['executable_path'] = '/usr/bin/clamscan';
# $config['clamav.settings']['mode_executable']['executable_parameters'] = '';

/**
 * Config directory settings.
 *
 * Configure the config sync directory after the Acquia default environment
 * settings to ensure they are configure to use the custom location which is
 * outside of the "docroot" and is managed by the code repository.
 */
$settings['config_sync_directory'] = '../config/common';

/**
 * Salt for one-time login links, cancel links, form tokens, etc.
 *
 * This variable will be set to a random value by the installer. All one-time
 * login links will be invalidated if the value is changed. Note that if your
 * site is deployed on a cluster of web servers, you must ensure that this
 * variable has the same value on each server.
 *
 * For enhanced security, you may set this variable to the contents of a file
 * outside your document root; you should also ensure that this file is not
 * stored with backups of your database.
 *
 * Fallback to Acquia recommended hash method. Don't prefer this as it's
 * expensive and potentially predictable.
 */
$settings['hash_salt'] = file_get_contents("/mnt/files/{$_ENV['AH_SITE_GROUP']}.{$_ENV['AH_SITE_ENVIRONMENT']}/keys/hash_salt.secret") ?: hash('sha256', $app_root . '/' . $site_path);

/**
 * Set the private files on Acquia environments.
 *
 * Ensure Acquia hosted environments are using the Acquia provided private
 * files directory. This path is outside of docroot, and curated by Acquia.
 */
$settings['file_private_path'] = "/mnt/gfs/{$_ENV['AH_SITE_GROUP']}.{$_ENV['AH_SITE_ENVIRONMENT']}/files-private";

/**
 * Set the Twig file cache directory.
 *
 * Normally this file is in the sites file directory but Drupal recommends to
 * store this outside of the docroot for added security. Twig will store
 * generated twig snippets and cache template contents here. It is possible
 * (though unlikely) some private data could be included.
 *
 * @see https://docs.acquia.com/acquia-cloud-platform/manage-apps/files/system-files
 */
$settings['php_storage']['twig']['directory'] = "/mnt/gfs/{$_ENV['AH_SITE_GROUP']}.{$_ENV['AH_SITE_ENVIRONMENT']}/tmp/php_storage";

/**
 * Acquia Environment Configurations.
 *
 * Enable modules and configurations that are available and should only be used
 * on Acquia hosted environments. These modules cause various warnings and
 * errors when enabled on non-acquia environments due to missing services.
 *
 * Acquia config split should provide:
 *   - Acquia Connector
 *   - Acquia Purge
 *   - Acquia Search (if utilized)
 *   - ClamAV (if utilized)
 *
 * Other configurations can also be included, and the config_split settings
 * should be reviewed on in the Web UI or from the config_split definitions.
 *
 * @see https://drupal.org/project/config_split
 */
$config['config_split.config_split.acquia']['status'] = true;

/**
 * Configuration overrides for non-prod Acquia environments.
 */
if ($_ENV['AH_SITE_ENVIRONMENT'] !== 'prod') {
  // Disallow robots on non-prod environments.
  $config['robotstxt.settings']['content'] = 'User-agent: *\r\nDisallow: /';
}

/**
 * Environment Indicator
 *
 * Provide default environment indicator settings and colors for the default
 * Acquia environments. These are used by the "drupal/environment_indicator"
 * module and it makes it easier to know which environment you are on quickly.
 *
 * @see https://drupal.org/project/environment_indicator
 */
switch($_ENV['AH_SITE_ENVIRONMENT']) {
  case 'prod':
    $config['environment_indicator.indicator']['name'] = 'Live';
    $config['environment_indicator.indicator']['fg_color'] = '#ffffff';
    $config['environment_indicator.indicator']['bg_color'] = '#999999';
    break;

  case 'test':
    $config['environment_indicator.indicator']['name'] = 'Staging';
    $config['environment_indicator.indicator']['fg_color'] = '#ffffff';
    $config['environment_indicator.indicator']['bg_color'] = '#990000';
    break;

  case 'dev':
    $config['environment_indicator.indicator']['name'] = 'QA';
    $config['environment_indicator.indicator']['fg_color'] = '#FFFFFF';
    $config['environment_indicator.indicator']['bg_color'] = '#990099';
    break;

  default:
    $config['environment_indicator.indicator']['name'] = 'Develop';
    $config['environment_indicator.indicator']['fg_color'] = '#ffffff';
    $config['environment_indicator.indicator']['bg_color'] = '#666600';
}
