# Common definitions & function for all the deploy hooks to utilize.
#
# The support for multi-site scripts relies on jq CLI tool which as of this
# writing Acquia provides version 1.5 on their servers. For multi-site, remember
# to provide
#


# Load the global hooks settings and site setup variables.
if [[ -f "${INCLUDES_DIR}/hook-settings.env" ]]; then
  . "${INCLUDES_DIR}/hook-settings.env"
fi

# Known folder locations
TMPDIR="/mnt/gfs/${AH_SITE_GROUP}.${AH_SITE_ENVIRONMENT}/tmp"
COMPOSER_BIN="$(composer config bin-dir --absolute --working-dir="${INCLUDES_DIR}/../../")"

#
# @param $1 - The needle.
# @param $2 - The haystack.
#
function inArray() {
  for ITEM in ${@:2}; do
    if [ $ITEM = $1 ]; then
      return 0
    fi
  done;

  return 1
}

#
# @param $1 - Name of the variable to use as the destination
# @param $2 - Array of values to join together into a single string.
# @param $3 - The join string, defaults to a single space character.
#
function arrayToStr() {
  local out=""
  local GLUE=${3:-' '}

  for STR in ${@:2}; do
    if [ -n $STR ]; then
      if [ -z $out ]; then
        out="( \\\"${STR}\\\""
      else
        out="${out}{$GLUE}\\\"${STR}\\\""
      fi
    fi
  done;

  eval "$1=\"${out} )\"";
}

#
# Wrapper for the Drush command.
#
# Acquia provides global Drush commands however they are older Drush
# implementations with the default being Drush 8. This function wraps the Drush
# installed in the project repository to ensure the Drush version included
# with the project is being used.
#
function drush() {
  if [ -f "${COMPOSER_BIN}/drush.php" ]; then
    php -d "memory_limit=1G" "${COMPOSER_BIN}/drush.php" $@
  else
    PHP_OPTIONS='-d "memory_limit=1G"' "${COMPOSER_BIN}/drush" $@
  fi
}

#
# Sets the SITE_URI variable to the URI matching the site and environment.
#
# @param $1 The name of the site to build the URI for.
# @param $2 The environment to build the URI for.
#
function setSiteUri() {
  if [[ ${IS_MULTISITE:-0} -eq 1 ]]; then
    if ! [[ -f "${INCLUDES_DIR}/sites.json" ]]; then
      echo "Missing \"sites.json\" file with the site settings for multi-site projects."; 1>&2
      exit 128
    fi

    # Try to fetch the site URI information from the sites definition.
    SITE_URI=$(jq -r '.sites[$site].environmentUri[$env] // empty' \
      --arg env "$2" \
      --arg site "$1" < "${INCLUDES_DIR}/sites.json")

    # If not site URI defined explicitly, try to resolve from URI pattern.
    if [[ -z "${SITE_URI}" ]]; then
      ENVIRONMENT_ALIAS=$(jq -r '.environments[$env].uri // empty' --arg env "$2" < "${INCLUDES_DIR}/sites.json")
      SITE_URI=$(jq -r '.sites[$site].uriPattern' --arg site "$1" < "${INCLUDES_DIR}/sites.json" \
        | sed -e "s/[{]alias_uri[}]/${ENVIRONMENT_ALIAS:-"{group}{env}"}/i" \
        | sed -e "s/[{]site[}]/$1/ig" -e "s/[{]env[}]/$2/ig" -e "s/[{]group[}]/${AH_SITE_GROUP}/ig" \
      )

      if [[ -z "${SITE_URI}" ]]; then
        return 1;
      fi
    fi

    return 0
  fi

  return 1
}

#
# Function to set the Drush CLI URI option based on the database name.
#
# This is meant to be used with DB related hooks which specify the individual
# database instead of the site the database belongs to. Acquia cloud platform
# recommends using their custom Drush command "ah-sql-cli" however this does
# not seem to be available on all environments.
#
# Appends URI settings to the DRUSH_ARGS variable.
#
# @param $1 - The database name, this argument is used by Acquia database hooks.
# @param $2 - The environment the URI is being generate for.
#
# @return 1 If the database is not a default Drupal database for the sites.
#
function setUriByDatabase() {
  if [[ "$1" = "${AH_SITE_GROUP}" ]]; then

    # Single site, or the default site database.
    return 0

  elif inArray $1 "${SITES[@]}"; then

    # If the database name matches a sitename, the use that as the site name.
    # NOTE: That this implies you cannot use a `site name` as the database name
    # for a site that doesn't match the `site name`.
    setSiteUri "$1" "$2"
    return 0

  elif [[ $3 -ne 1 && -f "${INCLUDES_DIR}/sites.json" ]]; then
    local site=$(jq -r '.sites|to_entries[]|select(.value.databases[]|contains($db))|.key' --arg db "$1" < "${INCLUDES_DIR}/sites.json")

    # Additional database belonging to a site
    setSiteUri "$site" "$2"
    return 0

  fi

  # Non-drupal table, this task can be safely skipped.
  return 1
}

#
# Load the Slack environment variables and setup the temp directory for Acquia
# web hooks using the an explicit site_group and site_environment provided
# from the calling hook.
#
# Normally we would us the "AH_SITE_GROUP" and "AH_SITE_ENVIRONMENT" variables
# provided but these are explicitly passed to HOOKs and the running context
# may not have these values available, so this should be use by all HOOKs when
# possible.
#
# @param $1 The project or site_group the HOOK is being executed for.
# @param $2 The site_environment that the HOOK is being executed for.
#
function loadEnvSettings() {
  local group=$1
  local env=$2

  # Load the per environment and secrets settings if available. The location is
  # recommended by Acquia https://docs.acquia.com/secrets#nobackup-directory and
  # will require an administrator to create and place this file if needed.
  local hookSettings="/mnt/gfs/${group}.${env}/nobackup/hook_settings.env"
  if [[ -f "${hookSettings}" ]]; then
    . "${hookSettings}"
  fi

  # If there is a notification type
  if [[ -n "${NOTIFY_API_TYPE}" && -f "${INCLUDES_DIR}/${NOTIFY_API_TYPE}-notify.sh" ]]; then
    echo "Setting the notification type: ${NOTIFY_API_TYPE}"
    . "${INCLUDES_DIR}/${NOTIFY_API_TYPE}-notify.sh"
  fi

  TMPDIR="/mnt/gfs/${group}.${env}/tmp"
}

#
# Default notify function, which only displays the message on the terminal.
#
# This is the default notification, and is expected to be overridden by loading
# the notification type configured by setting the $NOTIFY_API_TYPE environment
# variable.
#
# Think of this as the function signature to required by any notify method
# override to implement another notification method (see notify() in the
# slack-notify.sh file for an example).
#
# @param $1 A string value used as the status.
# @param $2 The "header" text value, the $NOTIFY_ICON is prepended to the text.
# @param $3 OPTIONAL response body text.
#
function notify() {
  echo "$1: $2"
  echo "-------------------------------------------"

  if [[ -n "${3:-}" ]]; then
    echo "${3}"
  fi
}

#
# Use the notify method to send an error notification and terminate the script.
#
# Use for when a deploy hook needs to report process errors before exitings.
#
# @param $1 The notification header title. Will have the error icon prepended.
# @param $2 The error log file that contains the script errors to report.
#
function notifyError() {
  local errMsg

  # Display the error, and then send it through the notification process.
  if [[ -n "$NOTIFY_API_TYPE" && "$NOTIFY_API_TYPE" != "none" ]]; then
    errMsg=$(<"$2")
  fi

  # Set the error display icon and get the error message.
  NOTIFY_ICON="${NOTIFY_ERROR_ICON:-:exclamation:}";
  notify "error" "$1" "${errMsg:-}"

  # Exit script with last error code.
  exit $?
}
