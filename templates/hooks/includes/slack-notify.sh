#!/bin/bash
#
# The Slack notification function relies heavily on "jq" CLI command in order to
# build the JSON payload for the webhook requests, and uses "cURL" command to
# send the request.
#
# If $SLACK_API_TOKEN and $SLACK_API_CHANNEL the chat.postMessage
# (https://api.slack.com/methods/chat.postMessage) API will be used to send the
# notification. This allows additional customization of the APP icon
# ($SLACK_API_EMOJI) and username ($SLACK_API_USER) is only available when using
# this API call. Take note of the required scopes in the documentation to ensure
# the Slack APP has permission to post using this API (recommend include -
# "chat:write", "chat:write.customize").
#
# Otherwise $SLACK_WEBHOOK_URL needs to be set to the Slack webhook URL in order
# to send the notification. Create a Slack webhook URL per enabled channel. App
# will write using the default App name and icon.
#


#
# Send a notification through a Slack App using a message API.
#
# @param $1 A string value used as the status.
# @param $2 The "header" text value, the $NOTIFY_ICON is prepended to the text.
# @param $3 The response body text, can be written using Slack Markdown syntax.
#
function notify() {
  local blocks="[]";

  addNotifyHeader "${NOTIFY_ICON:-:compass:} ${2}"
  addNotifyDivider

  # Add information fields for
  local fields='{ "type": "section", "fields": [] }'
  addNotifyField "Status" "${NOTIFY_ICON} ${1}"
  addNotifyField "Environment" "${SITE_ENV:-Acquia}"

  if [[ -n "${DEPLOY_TAG}" ]]; then
    if [[ -z "$DEPLOY_AUTHOR" ]]; then
      DEPLOY_AUTHOR="$(git show ${DEPLOY_TAG} --pretty='%cn (%ce)')"
    fi

    addNotifyField "Deployed" "${DEPLOY_TAG}"
    addNotifyField "Author" "${DEPLOY_AUTHOR}"
  fi

  if [[ -n "$NOTIFY_DASHBOARD_URI" ]]; then
    addNotifyField "Dashboard" "<${NOTIFY_DASHBOARD_URI}|${NOTIFY_DASHBOARD_LABEL:-View Dashboard}>"
  fi

  # Add the fields display to the blocks
  blocks=$(echo "$blocks" | jq ". + [${fields}]")
  addNotifyDivider

  if [ -n "$3" ]; then
    # If over the Slack text character limit trim thing down and keep the
    # tail end of the message which usually contains the error information.
    local text="$3"
    if [[ ${#text} -gt 3000 ]]; then
      text="...
$(echo "${text}" | tail -c 2990)"
    fi

    addNotifyBlock "Body" "${text}"
    addNotifyDivider
  fi

  # If Slack API token and channel are defined, use the chat.postMessage API.
  # Otherwise use the Webhook API if the webhook URL is available.
  if [[ -n "${SLACK_API_TOKEN}" && -n "${SLACK_API_CHANNEL}" ]]; then
    local jsonPayload="{}"

    if [[ -n "${SLACK_API_EMOJI}" ]]; then
      jsonPayload=$(echo "${jsonPayload}" | jq '. + {$icon_emoji}' --arg icon_emoji "$SLACK_API_EMOJI")
    fi

    if [[ -n "${SLACK_API_USER}" ]]; then
      jsonPayload=$(echo "${jsonPayload}" | jq '. + {$username}' --arg username "$SLACK_API_USER")
    fi

    curl -X POST "https://slack.com/api/chat.postMessage" \
      -H "Content-Type: application/json; charset=utf-8" \
      -H "Authorization: Bearer ${SLACK_API_TOKEN}" \
      -o - -s -w "\n%{http_code}\n" \
      --connect-timeout 3 \
      -d "$(echo ${jsonPayload} | jq -c '. + {$channel,$blocks}' --arg channel "${SLACK_API_CHANNEL}" --argjson blocks "${blocks}")"

  elif [[ -n "${SLACK_WEBHOOK_URL}" ]]; then

    curl -X POST "${SLACK_WEBHOOK_URL}" \
      -H "Content-Type: application/json; charset=utf-8" \
      -o - -s -w "\n%{http_code}\n" \
      --connect-timeout 3 \
      -d "$(jq -cn '{$blocks}' --argjson blocks "${blocks}")"
  fi
}

#
# Use the notify method to send an error notification and terminate the script.
#
# Use for when a deploy hook needs to report process errors before exitings.
#
# @param $1 The notification header title. Will have the error icon prepended.
# @param $2 The error log file that contains the script errors to report.
#
function notifyError() {
    # Set the error display icon and get the error message.
  NOTIFY_ICON="${NOTIFY_ERR_ICON:-:exclamation:}";

  local errMsg=$(<"$2");

  # Display the error, and then send it through the notification process.
  echo "$errMsg"
  notify "error" "$1" "${errMsg}"

  # Exit script with error code.
  exit 128
}

#
# Add a "section" content block to the Slack notification.
#
# @param $1 The block identifier (not visible).
# @param $2 The block text content.
# @param $3 The block text formatting type (defaults to "mrkdwn").
#
function addNotifyBlock() {
  blocks=$(echo "$blocks" | jq '. += [{$type, $block_id, $text}]' \
    --arg type "section" \
    --arg block_id "$1" \
    --argjson text "$(jq -n '{$type, $text}' \
      --arg type "${3:-mrkdwn}" \
      --arg text "${2}" \
    )" \
  )
}

#
# Creates a divider block in the Slack notification.
#
function addNotifyDivider() {
  blocks=$(echo "$blocks" | jq '. += [{"type": "divider"}]')
}

#
# Creates a header block for the notification. Header blocks are limited to
# 150 characters and only allows for "plain_text" formatting.
#
# @param $1 The heading text to display. Needed to be plain text but can have emojis.
#
function addNotifyHeader() {

  # Trim header if over the text limit.
  local text="$1"
  if [[ ${#text} -gt 150 ]]; then
    text="$(echo "${text}" | head -c 145)..."
  fi

  blocks=$(echo "$blocks" | jq '. += [{$type, $text}]' \
    --arg type "header" \
    --argjson text "$(jq -n '{$type, $text}' \
      --arg type "plain_text" \
      --arg text "${text}" \
    )" \
  )
}

#
# Adds a block.fields[] object to the "$fields" environment variable. The
# "$fields" is assumed to have been created by the caller and should be a Slack
# block with a "fields" array property (@see function notify()).
#
# @param $1 Is the title text to display for the field.
# @param $2 The text value for the field.
# @param $3 The text format type to use for rendering the field (defaults to: mrkdwn)
#
function addNotifyField() {
  fields=$(echo "$fields" | jq '.fields += [{$type, $text}]' \
    --arg type "${3:-mrkdwn}" \
    --arg text "*$1*
$2")
}
