#!/bin/bash
#
# Use to sanitize Drupal default database after it has been copied from another
# environment. If the environment is not PROD, then it will have its tables
# sanitized.
#
# The following tables are sanitized if they exist:
# ======= All environments
# - batch
# - semaphore
# - sessions
# ======= Non-prod environments
# - history
# - watchdog
# - webform_submission
# - content_lock
# - encrypted_field
# - Any table listed in $DB_TRUNC_TABLES array
#

set -e

# Ensure a common reference directory by the location of this script.
INCLUDES_DIR="$(dirname "$(readlink -f "$0")")/../../includes"

# Load the shared base common functions and settings.
. "${INCLUDES_DIR}/base.sh"


#
# Copy script arguments into var names for better clarity.
#
# -----
# NOTE:
# -----
# db_name ($3) is not the actual MySQL database name but rather the common name
# for the database in all environments.
#
# @see https://github.com/acquia/cloud-hooks/tree/master?tab=readme-ov-file#post-db-copy
#
project=$1
env=$2
db_name=$3
source=$4

# Setup a place for the Drush arguments to get built.
DRUSH_ARGS='';

# Find and build Drush arguments for the site. Will exit if not the default
# Drupal database - since this sanitization is only relevant to Drupal tables.
if setUriByDatabase "$db_name" "$env" 1; then
  # Get a list of currently installed database tables.
  DB_TABLES=$(echo "show tables;" | drush "@$project.$env" $DRUSH_ARGS sqlc)

  # List of tables to truncate if they exist.
  tables=("batch" "semaphore" "sessions")

  # For non-prod environments additionally remove submission data.
  # Add other private data, and tables that should be removed.
  if [[ "$env" != "prod" && "$source" = "prod" ]]; then
    tables+=("history" "watchdog" "webform_submission")

    # Additional tables to sanitize during copying of databases.
    if [[ ${#DB_TRUNC_TABLES[@]} -gt 0 ]]; then
      tables+=("${DB_TRUNC_TABLES[@]}")
    fi
  fi

  # Scrub data from tables the imported database that should not be imported
  # from the source environment.
  sql="";
  for table in "${tables[@]}"; do
    if [[ -n $(echo "$DB_TABLES" | grep "^${table}\$") ]]; then
      sql="${sql}
TRUNCATE table ${table};"
    fi
  done

  if [[ -n "$sql" ]]; then
    echo "$sql" | drush "@$project.$env" $DRUSH_ARGS sqlc
  fi
fi
