#!/bin/bash
#

set -eo pipefail

# Ensure a common reference directory by the location of this script.
INCLUDES_DIR="$(dirname "$(readlink -f "$0")")/../../includes"

# Load the shared base common functions and settings.
. "${INCLUDES_DIR}/base.sh"


# Copy script arguments into var names for better clarity.
project=$1
env=$2
db_name=$3
source=$4

# Setup a place for the Drush arguments to get built.
DRUSH_ARGS='';

#
# Encapsulates the deploy script actions in a sub-shell so errors can be caught
# and reported on. Utilizes the global environment variables for
#  - $DRUSH_ARGS
#
# @param $1 The project or site_group the HOOK is being executed for.
# @param $2 The site_environment that the HOOK is being executed for.
#
function updateDbScripts() {(
  set -e

  local alias="@${1}.${2}";

  # Ensure that Drupal update processes are run on the target environment.
  # Generally we should be applying the current environment code configs but
  # want to setup conditionals in the future if required.
  drush "${alias}" $DRUSH_ARGS cr 2> /dev/null || true
  drush "${alias}" $DRUSH_ARGS updb -y && \
  drush "${alias}" $DRUSH_ARGS cim -y && \
  drush "${alias}" $DRUSH_ARGS cr && \
  drush "${alias}" $DRUSH_ARGS deploy:hook -y || exit 1

  # If search_api module is installed, clear and re-index content because the
  # content has changed after a database copy.
  search_api=$(drush "${alias}" $DRUSH_ARGS pm-list --format=list --type=module --status=enabled --no-core | grep "search_api")
  if [[ -n $search_api ]]; then
    drush "${alias}" $DRUSH_ARGS sapi-c && \
    drush "${alias}" $DRUSH_ARGS sapi-i || exit 1
  fi
)}

# Find and build Drush arguments for the site. Will exit if not the default
# database for one of the multi-site environments.
if setUriByDatabase "${db_name}" "${env}" 1; then
  DRUSH_ARGS="$DRUSH_ARGS --uri=$SITE_URI"

  loadEnvSettings "$project" "$env"

  DEPLOY_LOG="${TMPDIR}/${project}-${env}_updatedb.log"
  DEPLOY_AUTHOR="Acquia DB hooks"
  updateDbScripts "${project}" "${env}" 2>&1 | tee "${DEPLOY_LOG}" || notifyError "Failed database post-copy: ${db_name}" "${DEPLOY_LOG}"

  # If notifyError did not get called and exits, we can report success!
  NOTIFY_ICON="${NOTIFY_SUCCESS_ICON:-:tada:}"
  notify "Success" "Database \"$db_name\" copy from ${source} successful"
fi
