#!/bin/bash
#

set -eo pipefail

# Ensure a common reference directory by the location of this script.
INCLUDES_DIR="$(dirname "$(readlink -f "$0")")/../../includes"

# Load the shared base common functions and settings.
. "${INCLUDES_DIR}/base.sh"


# Copy script arguments into var names for better clarity.
project=$1
env=$2
branch="$3"
tag="$4"
repo_url="$5"
repo_type="$6"

#
# Run Drush deploy scripts on a site environment in a sub-shell.
#
# Function is set to exit on first Drush command failure and report it through
# the notifyError() function.
#
# @param $1 The project or site_group the HOOK is being executed for.
# @param $2 The site_environment that the HOOK is being executed for.
# @param $3 The site (in the multi-site) the deploy scripts are executing on.
#
function deploySite() {(
  set -e

  local alias="@${1}.${2}";

  # Ensure that Drupal update processes are run on the target environment.
  drush "${alias}" $DRUSH_ARGS cr 2> /dev/null || true

  # Exit with error if any of these Drush commands fail.
  drush "${alias}" $DRUSH_ARGS updb -y && \
  drush "${alias}" $DRUSH_ARGS cim -y && \
  drush "${alias}" $DRUSH_ARGS cr && \
  drush "${alias}" $DRUSH_ARGS deploy:hook -y || exit 1
)}

# Start the deploy hooks for each of the sites
loadEnvSettings "$project" "$env"
DEPLOY_LOG="${TMPDIR}/${project}-${env}_deploy.log"
DEPLOY_TAG="${tag:-$branch}"
DEPLOY_AUTHOR="Acquia Deploy Hook"

# Execute the deployment actions based on if this is a multi-site.
if [[ ${IS_MULTISITE:-0} == 0 && ${#SITES[@]} -le 1 ]]; then

  DRUSH_ARGS="";
  deploySite "${project}" "${env}" 2>&1 | tee "${DEPLOY_LOG}" || notifyError "Deploy to $env failed" "${DEPLOY_LOG}"
else

  for site in "${SITES[@]}"; do
    printf "Deploying ${site} for ${env}\n"
    printf "======================================\n\n"

    # Multi-sites are defined in the /docroot/sites/sites.php file. Use this
    # to each of the sites on the environment.
    DRUSH_ARGS=""
    if setSiteUri "${site}" "${env}"; then
      DRUSH_ARGS="${DRUSH_ARGS} --uri=${SITE_URI}"
    fi

    deploySite "${project}" "${env}" 2>&1 | tee "${DEPLOY_LOG}" || notifyError "Deploy to ${env} failed for site: ${site}" "${DEPLOY_LOG}"
  done;
fi

# Report successful deploy.
NOTIFY_ICON="${NOTIFY_SUCCESS_ICON:-:tada:}"
notify "success" "Deploy to ${env} successful"
