# Raini Acquia Extension

This extension adds Acquia hosting settings and recommended Drupal modules for
Acquia Cloud hosting.


## Install

This extension requires the `raini/drupal` Composer package, which
provides a setup for a standard Drupal website. In order to be compatible with
the Acquia hosting, the Drupal project settings to use `memcache` for the
caching type, as this is what is provided on Acquia's Cloud hosting.


## Configurations

  * **ClamAV** (bool) - Add settings for ClamAV and include the Drupal module.
  * **solrSearch** (bool) - Include and setup Acquia Search.
  * **includeHooks** (init, overwrite, false) - Include the extension's default
    Acquia cloud hooks.
  * **notifyType** (string) - Name of the API in /hooks/includes to use when
    sending notifications for errors or status messages. Defaults to "none"
    which uses the default terminal display only.
  * **useConfigSplit** (init, overwrite, false) - Include Acquia environment
    config_split configurations (also installs `drupal/config_split`).


Example configurations (raini.project.yml):

```
extensions:
    raini-acquia:
        clamAV: true
        solrSearch: true
        includeHooks: init
        notifyType: slack
        useConfigSplit: init
```


## Included Packages

Additional Composer packages are brought to the Drupal site installation

These packages are added to each install by this extension:

  * Acquia Environment Detector (`acquia/drupal-environment-detector`)
  * Acquia Connector (`drupal/acquia_connector`)
  * Acquia Purge (`drupal/acquia_purge`)

The Acquia Search (`drupal/acquia_search`) module is conditionally installed
if the project is using, and has installed Search API Solr
(`drupal/search_api_solr`) as this is a requirement for the Acquia Search
module, but also an indication the intention to use Search API and Apache Solr
to implement search (`raini/drupal-solr` is recommended for local setup).

THe use of the following is highly recommended:

  * Config Ignore (`drupal/config_ignore`)
  * Config Split (`drupal/config_split`)

These can help to separate Acquia modules into `config_split` to only be enabled
on hosted Acquia servers and not on local development environments. You may also
want to further split your configurations for development and production
environments.

The following recommended splits:
  * acquia - Acquia hosted environments: Acquia Connector, Acquia Purge,
    Acquia Search.
  * prod - Configurations for live site: GTM
  * local - Local development tools like: develop, staged_file_proxy


## Multi-site Setup

For multi-site installations on Acquia you should populate the
`/hooks/includes/sites.json` file with the relevant database and site URI
information for each site. JSON format was choosen since environment variable
(shell & .env files) do not support structured data or nested arrays and Acquia
provides `jq` for working with JSON data through the commandline.

**Example** sites.json:
```
{
  "site1": {
      "uriPattern": "{env}-{site1}.prod.{group}.acquia-sites.com",
      "databases": [
        "site1"
      ]
  },
  "site2": {
    "environmentUri": {
      "dev": "unique-dev.domain.com",
      "test": "staging.domain.com",
      "prod": "www.production.com"
    },
    "databases": [
      "site2",
      "extra_database",
    ]
  }
}
```


### Site URI Settings

The `sites.json` should have a either a `uriPattern` or `environmentUri` field
that define the site URIs for each of the environments.

The `uriPattern` is a single string value with placeholders for the environment
("{env}"), site ("{site}") and site group ("group") that get replaced by their
respective values from the current context. This works well for setups that
follow a pattern for most environments.

The `environmentUri` is an object with the keys as the environment name, and
has the values as the URI to use for that environment. When available for the
current environment this explicit URI is preferred and checked for first. This
allows you the flexibility to only set a unique URL for production environments
but allow the pattern to be applied fof all others.

**Example:**
```
{
  // This uses "www.production.com" for the production environment for this
  // site, and uses the URI pattern to generate the URIs for all other
  // environments.
  "site1": {
    "uriPattern": "{env}-{site1}.prod.{group}.acquia-sites.com",
    "environmentUri": {
      "prod": "www.production.com"
    }
  }
}
```


### Site Databases

Drupal sites can have multiple database connections and the Acquia deploy hooks
only provide the database names. Providing a mapping of sites to databases can
allow these hooks to determine which sites the database being copied or restored
belongs to.

This is only needed for databases that don't match the site ID. The database
names that match the a site ID or site group name are automatically assumed to
be the default Drupal database for the site with a matching name.

**Example:**
```
{
  "site1": {
    "databases": [
      "additional-db"
    ]
  },
  "site2": {
    "databases": [
      "db1",
      "db2"
    ]
  }
}
```
