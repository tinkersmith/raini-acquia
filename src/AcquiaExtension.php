<?php

/*
 * This file is part of the Raini Acquia package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Acquia;

use Raini\Core\Configurator\ExtensionSchemaConfigurator;
use Raini\Core\Extension\AbstractExtension;
use Raini\Core\Extension\ComposerExtensionInterface;
use Tinkersmith\Configurator\Attribute\Configurator;

/**
 * Acquia configurations and packages for supporting a Drupal site.
 *
 * Adds support a Drupal projected hosted with Acquia (https://acquia.com), this
 * includes PHP settings, web hooks, and additional Drupal modules.
 */
#[Configurator(ExtensionSchemaConfigurator::class, 'raini-acquia', 'config/acquia.schema.yml')]
class AcquiaExtension extends AbstractExtension implements ComposerExtensionInterface
{

    /**
     * {@inheritdoc}
     */
    public function getRepositories(): array
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function getDevPackages(): array
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function defaultSettings(): array
    {
        return [
            'clamAV' => true,
            'solrSearch' => true,
            'includeHooks' => 'init',
            'notifyType' => 'none',
            'useConfigSplit' => 'init',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getPackages(): array
    {
        $packages = [
            'acquia/drupal-environment-detector',
            'drupal/acquia_connector',
            'drupal/acquia_purge',
            'drupal/http_cache_control',
            'drupal/memcache',
        ];

        if (!empty($this->settings['clamAV'])) {
            $packages[] = 'drupal/clamav';
        }

        // If Acquia Search is used, this will also require "drupal/search_api"
        // and "drupal/search_api_solr" modules. I recommend using the
        // "tinkersmith/drupal-solr" Raini extension to help with the Solr
        // environment settings, however that isn't required.
        //
        // @see ./templates/settings.acquia.php
        if (!empty($this->settings['solrSearch'])) {
            $packages[] = 'drupal/acquia_search';
        }

        // Adds the config_split Drupal module, and during project generation
        // will add baseline Acquia environment configuration split settings.
        if (!empty($this->settings['useConfigSplit'])) {
            $packages[] = 'drupal/config_split';
        }

        return $packages;
    }
}
