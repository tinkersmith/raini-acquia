<?php

/*
 * This file is part of the Raini Acquia package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Raini\Acquia;

use Raini\Core\Environment;
use Raini\Core\File\PathHelper;
use Raini\Core\Project\Tenant;
use Raini\Core\Project\GenerateOptions;
use Raini\Core\Project\Generator\GeneratorTrait;
use Raini\Core\Project\TenantGeneratorInterface;
use Raini\Core\Utility\EnvValues;
use Raini\Drupal\DrupalSite;
use Raini\Drupal\DrupalTenant;
use Raini\Drupal\Event\DrupalEvents;
use Raini\Drupal\Event\DrupalSiteSettingsEvent;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Filesystem\Filesystem;
use Tinkersmith\SettingsBuilder\Php\Expr\Assignment;
use Tinkersmith\SettingsBuilder\Php\Expr\Expression;
use Tinkersmith\SettingsBuilder\Php\Expr\InvokeExpression;
use Tinkersmith\SettingsBuilder\Php\Expr\PhpValue;
use Tinkersmith\SettingsBuilder\Php\SettingsBuilder;
use Tinkersmith\SettingsBuilder\Php\Stmt\Conditional;
use Tinkersmith\SettingsBuilder\Php\Stmt\Statement;

/**
 * The Acquia project generator.
 *
 * Creates the Acquia web hooks and adds Acquia specific settings to the Drupal
 * settings.php files.
 */
class AcquiaGenerator implements TenantGeneratorInterface, EventSubscriberInterface
{
    use GeneratorTrait;

    /**
     * @param AcquiaExtension $extension
     * @param Environment     $env
     * @param PathHelper      $pathHelper
     */
    public function __construct(protected AcquiaExtension $extension, protected Environment $env, protected PathHelper $pathHelper)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function getEnv(): Environment
    {
        return $this->env;
    }

    /**
     * {@inheritdoc}
     */
    public function isApplicable(Tenant $tenant, GenerateOptions $options): bool
    {
        return $tenant instanceof DrupalTenant;
    }

    /**
     * {@inheritdoc}
     *
     * @param DrupalTenant $tenant
     */
    public function runForTenant(Tenant $tenant, GenerateOptions $options, ?OutputInterface $output = null): void
    {
        if ($output) {
            $msg = 'Creating Acquia hooks & settings files...';
            $output instanceof SymfonyStyle ? $output->section($msg) : $output->writeln("<info>$msg</>");
        }

        $fs = new Filesystem();
        $settings = $this->extension->getSettings();

        // If configured add the Acquia hooks template, copy hooks and create
        // settings files based on the current site configurations.
        if (!empty($settings['includeHooks'])) {
            $hookPath = $tenant->normalizePath('hooks');

            if ('overwrite' === $settings['includeHooks'] || !file_exists($hookPath)) {
                $fs->mirror($this->getTemplatePath('hooks'), $hookPath);

                $hookSettingPath = $hookPath.'/includes/hook-settings.env';
                $hookSettings = file_exists($hookSettingPath) ? EnvValues::createFromFile($hookSettingPath) : new EnvValues();
                if ($tenant->isMultiSite()) {
                    $hookSettings['IS_MULTISITE'] = 1;
                    $hookSettings['SITES'] = array_keys($tenant->getSites());

                    // Provide a scaffolded "sites.json".
                    if (!file_exists($hookPath.'/includes/sites.json')) {
                        file_put_contents($hookPath.'/includes/sites.json', json_encode([
                            'environments' => [
                                'test' => ['name' => 'stage', 'uri' => '{group}stg'],
                                'prod' => ['name' => 'prod', 'uri' => '{group}'],
                            ],
                            'sites' => array_fill_keys(array_keys($tenant->getSites()), [
                                'uriPattern' => '{site}.{alias_uri}.prod.acquia-sites.com',
                                'environmentUri' => ['prod' => null],
                                'databases' => [],
                            ]),
                        ], JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE|JSON_INVALID_UTF8_IGNORE|JSON_UNESCAPED_SLASHES));
                    }
                } else {
                    $hookSettings['IS_MULTISITE'] = 0;
                }

                $hookSettings['NOTIFY_API_TYPE'] = $settings['notifyType'] ?: $hookSettings['NOTIFY_API_TYPE'] ?? 'none';
                $hookSettings->write($hookSettingPath, <<<EOT
                    # Settings for the Acquia hooks.
                    #
                    # Loaded by the "/hooks/includes/base.sh" file and contains global settings.
                    # You can store other global variables here but avoid saving environment
                    # specific settings or secure keys here.
                    #
                    # For environment specific or private settings use the *.env file:
                    # "/mnt/gfs/\${AH_SITE_GROUP}.\${AH_SITE_ENVIRONMENT}/nobackup/hook_settings.env"
                    #
                    # Make sure to call loadEnvSettings() before using the values or the notify().
                    #
                    # @see loadEnvSettings() in /hooks/includes/base.sh
                    EOT
                );
            }
        }

        $sites = $tenant->getSites();
        if ($tenant->isMultiSite()) {
            $filepath = '/mnt/gfs/{$_ENV[\'AH_SITE_GROUP\']}.{$_ENV[\'AH_SITE_ENVIRONMENT\']}/{$site_path}';

            // Ensure a default site is available for fallback purposes.
            if (empty($sites['default'])) {
                $sites['default'] = reset($sites);
            }
        } else {
            // Only a single site, just create it using the 'default' folder.
            $sites = ['default' => reset($sites)];
            $filepath = '/mnt/gfs/{$_ENV[\'AH_SITE_GROUP\']}.{$_ENV[\'AH_SITE_ENVIRONMENT\']}';
        }

        // Location of the Acquia memcache settings relies on the location of
        // vendor directory. Resolve from the vendor location in case.
        $bltFile = $tenant->getVendorDir().'/acquia/blt/settings/blt.settings.php';
        $bltFile = $this->pathHelper->makeRelative($bltFile, $tenant->getDocroot());
        $memcacheFile = $tenant->getVendorDir().'/acquia/memcache-settings/memcache.settings.php';
        $memcacheFile = $this->pathHelper->makeRelative($memcacheFile, $tenant->getDocroot());
        $saltPath = '/mnt/gfs/{$_ENV[\'AH_SITE_GROUP\']}.{$_ENV[\'AH_SITE_ENVIRONMENT\']}/nobackup/.keys';

        /** @var DrupalSite $site */
        foreach ($sites as $name => $site) {
            if ($output) {
                $output->writeln(" - Acquia files for the <info>$name</info> site");
            }

            $builder = new SettingsBuilder();
            $builder->assignValue('$acquia_blt_settings', new PhpValue($bltFile));
            $builder->assignValue('$acquia_memcache_settings', new PhpValue($memcacheFile));
            $saltFile = ('default' === $name) ? 'hash_salt.secret' : "hash_salt.{$name}.secret";

            // Force the config sync directory value, this happens after the
            // Acquia default settings are loaded, and ensures we are loading
            // configurations at the folder we designated for the project site.
            $relConfigDir = $this->pathHelper->makeRelative($site->getConfigDir(), $tenant->getDocroot());
            $builder->assignValue("\$settings['config_sync_directory']", $relConfigDir);
            $builder->assignValue("\$settings['file_private_path']", new Expression("\"$filepath/files-private\""));
            $builder->assignValue("\$settings['hash_salt']", new Expression("file_get_contents(\"{$saltPath}/{$saltFile}\") ?: hash('sha256', \$app_root . '/' . \$site_path)"));

            // Enable the ClamAV features if enabled.
            if (!empty($settings['clamAV'])) {
                $builder->assignValue("\$config['clamav.settings']['scan_mode']", 1);
                $builder->assignArray("\$config['clamav.settings']['mode_executable']", [
                    'executable_path' => '/usr/bin/clamscan',
                    'executable_parameters' => '',
                ], 1);
            }

            // Write the Acquia settings file.
            $builder->writeFile($tenant->getDocroot()."/sites/{$name}/settings.acquia.php", $this->getTemplatePath('settings.acquia.php'));
        }
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents(): array
    {
        return [
            DrupalEvents::DRUPAL_SITE_SETTINGS => [
                ['onDrupalSiteSettings', 0],
            ],
        ];
    }

    /**
     * Event handler for altering the Drupal sites settings building.
     *
     * Allows Acquia extension to add Acquia environment settings, to the
     * host conditional group.
     *
     * @param DrupalSiteSettingsEvent $event The Drupal sites settings event with the sites info and the SettingsBuilder
     *                                       used to generate the settings.
     */
    public function onDrupalSiteSettings(DrupalSiteSettingsEvent $event): void
    {
        if (!($hostGroup = $event->getHostingGroup())) {
            return;
        }

        $siteName = $event->getSite()->getName();
        $settingsPrefix = $event->getTenant()->isMultiSite() && ('default' !== $siteName)
            ? $siteName : '{$_ENV[\'AH_SITE_GROUP\']}';

        // Add the Acquia settings includes if the hosting environment variables
        // are detected. This gets included in Drupal sites settings.php files.
        $acquiaCondition = new Conditional(
            new Expression("isset(\$_ENV['AH_SITE_ENVIRONMENT'])"),
            [
                new Conditional(
                    new Expression("file_exists('/var/www/site-php')"),
                    [
                        new Statement('global $conf, $databases'),
                        new Statement(new Assignment("\$conf['acquia_hosting_settings_autoconnect']", false), "\n// Do not immediately connect to database on Acquia Cloud."),
                        new Statement('require "/var/www/site-php/{$_ENV[\'AH_SITE_GROUP\']}/'.$settingsPrefix.'-settings.inc"'),
                        new Statement(
                            new Assignment("\$databases['default']['default']['init_commands']", [
                                'tx_isolation' => "SET SESSION TRANSACTION ISOLATION LEVEL READ COMMITTED",
                            ]),
                            <<<EOT

                                /**
                                 * Set the transation isolation level to READ_COMMITTED.
                                 *
                                 * For compatibility, Acquia default sets the isolation level to REPEATABLE_READ
                                 * but this can on occasion cause deadlocks when using semaphore tables. This set
                                 * the isolation level to READ COMMITTED as recommended for Drupal 10.1 and newer.
                                 *
                                 * @see https://docs.acquia.com/acquia-cloud-platform/manage-apps/database/mysql-transactions
                                 */
                                EOT
                        ),
                        new Conditional(
                            new Expression("function_exists('acquia_hosting_db_choose_active')"),
                            new Statement(new InvokeExpression('acquia_hosting_db_choose_active')),
                            "\n// When availble, allow Acquia to select the active database for the current site."
                        ),
                    ],
                    <<<EOT
                        /**
                         * Include the Acquia environment settings file.
                         *
                         * Acquia provides the following settings from this file:
                         *   - Establishes the trusted host patterns for Drupal.
                         *   - Establishes the Memcached infrastructure and connection information.
                         *   - Builds all of the database connection information.
                         *   - Suppresses error reporting in the production environment.
                         *
                         * Override any of the Acquia provided settings (e.g. the config directory)
                         * options in the "settings.acquia.php" file.
                         *
                         * @see https://docs.acquia.com/cloud-platform/manage/code/require-line/
                         */
                        EOT
                ),
                new Statement(
                    'require "{$app_root}/{$site_path}/settings.acquia.php";',
                    '// Project settings to override and extend the Acquia hosting defaults.'
                ),
            ],
            <<<EOT
                /**
                 * Adds settings when Acquia hosting environment is detected.
                 *
                 * add the Acquia specific configurations and settings.
                 *
                 * @see https://docs.acquia.com/cloud-platform/develop/env-variable/
                 */
                EOT
        );

        $hostGroup->addCondition($acquiaCondition);
    }
}
